package red.yukiisbo.randomspawn;

import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public class Plugin extends JavaPlugin {

  private Random random;

  private World world;
  private int lowerBound;
  private int upperBound;
  private int innerBound;

  @Override
  public void onEnable() {
    saveDefaultConfig();

    random = new Random();

    loadConfig();
    registerListeners();
  }

  private void loadConfig() {
    lowerBound = getConfig().getInt("lowerbound", 1000);
    upperBound = getConfig().getInt("upperbound", 10000);

    innerBound = upperBound - lowerBound;

    String worldName = getConfig().getString("world", "world");
    world = getServer().getWorld(worldName);
  }

  private void registerListeners() {
    getServer().getPluginManager().registerEvents(new JoinListener(this), this);
  }

  private int calculateRandom() {
    return lowerBound + random.nextInt(innerBound);
  }

  public Location calculateLocation() {
    Location loc;

    do {
      int x = calculateRandom();
      int z = calculateRandom();
      int y = world.getHighestBlockYAt(x, z);

      loc = new Location(world, x, y, z);
    } while (world.getBlockAt(loc).getType() == Material.WATER ||
        world.getBlockAt(loc).getType() == Material.LAVA);

    loc.add(0, 1, 0);

    return loc;
  }
}
