package red.yukiisbo.randomspawn;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

  private Plugin instance;

  public JoinListener(Plugin instance) {
    this.instance = instance;
  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent e) {
    Player p = e.getPlayer();

    if (p.hasPlayedBefore()) {
      return;
    }

    Location spawnLocation = instance.calculateLocation();

    p.teleport(spawnLocation);
    p.setBedSpawnLocation(spawnLocation, true);
  }
}
